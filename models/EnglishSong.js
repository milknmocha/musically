const Sequelize = require('sequelize')
const sequelize = require('../util/database')

const Model = Sequelize.Model
class EnglishSong extends Model {}

EnglishSong.init(
    {
        id: {
            type: Sequelize.STRING(16),
            autoIncrement: false,
            allowNull: false,
            primaryKey: true
        },
        title: { type: Sequelize.TEXT, allowNull: false },
        thumbnail: { type: Sequelize.TEXT, allowNull: false },
        url: { type: Sequelize.TEXT, allowNull: false },
       
    },
    {
        sequelize,
        modelName: 'EnglishSong',
        tableName: 'english_songs',
        timestamps: true
    }
)

module.exports = EnglishSong
