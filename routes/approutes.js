const express = require('express')
const { body } = require('express-validator')

const router = express.Router()
const Controller = require('../controllers/app/UserController')
const { Router } = require('express')

router.post('/get-all-song', Controller.getAllSongs)
router.post('/get-url', Controller.getSongUrl)
module.exports = router
