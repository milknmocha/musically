const Sequelize = require('sequelize')
const Op = Sequelize.Op
const EnglishSong = require('../../models/EnglishSong')
const HindiSong = require('../../models/HindiSong')
const apiResponder = require('../../util/responder')
const errorhandler = require('../../util/errors')
const { request, response } = require('express')

exports.getAllSongs = async( request,response,next)=>{
    try{
        if(request.body.song_type == 'ENG'){
            let Song = await EnglishSong.findAll({
                attributes: ['title','id','duration'],
                limit: request.body.limit,
                offset: request.body.offset
            })
            if(!Song) throw errorhandler.createError(1000)
            return apiResponder(request,response,next, true,2000,{Song})
        }
        if(request.body.song_type == 'HIN'){
            let Song = await HindiSong.findAll({
                attributes: ['title','id','duration'],
                limit: request.body.limit,
                offset: request.body.offset
            })
            if(!Song) throw errorhandler.createError(1000)
            return apiResponder(request,response,next, true,2000,{Song})
        }   
    }
    catch(error){
        next(error)
    }

}

exports.getSongUrl= async(request,response,next)=>{
    try{
        let url;
        let engUrl = await EnglishSong.findOne({
            attributes: ['url','thumbnail','title','duration'],
            where: {
                id: request.body.song_id
            }
        })
        if(!engUrl){
            let hindiUrl =await HindiSong.findOne({
                attributes: ['title','thumbnail','url','duration'],
                where: {
                    id: request.body.song_id
                }
            })
            if(!hindiUrl) throw errorhandler.createError(1000)

            url = hindiUrl;
        }
        else
            url= engUrl;

        return apiResponder(request,response,next,true,2001, { URL: url })
        
    }
    catch(error){
        next(error)
    }
}



