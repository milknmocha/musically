const AccountOtp = require('../models/AccountOtp')

exports.parse = async data => {
    const dataStringify = await JSON.stringify(data)
    const parsing = await JSON.parse(dataStringify)
    return parsing
}

exports.sendOtp = async (id,type) => {
    let date = new Date().getTime()
    date += 1 * 60 * 5 * 1000
    const findExistingOtp = await AccountOtp.findOne({
        where: { account_id: id, account_type: type }
    })
    if (!findExistingOtp) {
        await AccountOtp.create({
            account_id: id,
            account_type: type,
            otp: 123456,
            expiry_date: date
        })
    }
    
    await AccountOtp.update({
        expiry_date: date
    },
    {
        where: {account_id: id, account_type: type}
    })
}

exports.generateRandomString = async (length = 6) => {
    let characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let charactersLength = characters.length
    let randomString = ''
    for (let i = 0; i < length; i++) {
        randomString += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return randomString
}
