const User =  require('../../models/user')
const Collector = require('../../models/Collector')
const errorHandler = require('../../util/errors')
const bcrypt = require('bcryptjs')
const ValidateEmail = require('../../util/validation/isAlreadyEmail')
const apiResponder = require('../../util/responder')
const general = require('../../util/general')
const jwt = require('jsonwebtoken')

exports.registerWithEmail = async (request, response, next) => {
    try {
        let id,type
        if (errorHandler.validate(['first_name','last_name', 'email', 'password','type'], request.body))
            return apiResponder(request, response, next, true, 2003, {})

        const isEmailExists = await ValidateEmail.isEmailExist(request.body.email)
        if (isEmailExists == false) {
            throw errorHandler.createError(1009)
        }

        const hashPwd = await bcrypt.hash(request.body.password, 12)
        if(request.body.type == 'U'){
            const user = await User.create({
                    first_name:request.body.first_name,
                    last_name: request.body.last_name,   
                    email: request.body.email,
                    password: hashPwd
                })
                if (!user) {
                    throw errorHandler.createError(1006)
                }
                
            await general.sendOtp(user.id, 'U')
            
            id = user.id
            type = 'U'

        }
        else if(request.body.type == 'C'){
            const collector = await Collector.create({
                    first_name:request.body.first_name,
                    last_name: request.body.last_name,   
                    email: request.body.email,
                    password: hashPwd
                })
                if (!collector) {
                    throw errorHandler.createError(1007)
                }
                
            await general.sendOtp(collector.id, 'C')
            
            id = collector.id
            type = 'C'

        }
         return apiResponder(request, response, next, true, 2006, {id: id,type: type })
    } catch (err) {
        next(err)
    }
}


exports.registerWithSocialMedia = async (request, response, next) => {
    try {
        let id,type
        if (errorHandler.validate(['first_name','last_name', 'email'], request.body))
            return apiResponder(request, response, next, true, 2003, {})
          const isEmailExists = await ValidateEmail.isEmailExist(request.body.email)
            if (isEmailExists == false) {
                throw errorHandler.createError(1009)
            }
            if(request.body.type == 'U'){
                const user = await User.create({
                    first_name:request.body.first_name,
                    last_name: request.body.last_name,
                    email: request.body.email,
                    is_verified: 1
                 })
                if (!user) {
                    throw errorHandler.createError(1006)
                }
                id = user.id
                type = 'U'
            }
            if(request.body.type == 'C'){
                const collector = await Collector.create({
                    first_name: request.body.first_name,
                    last_name: request.body.last_name,
                    email: request.body.email,
                    is_verified: 1
                })
        
                if(!collector){
                    throw errorHandler.createError(1007)
                }
                id = collector.id
                type = 'C'
            }  
            return apiResponder(request, response, next, true, 2006, {id: id, type: type})
        }catch(error){
            next(error);
        }
}

exports.loginWithEmail = async (request, response, next) =>{
    try{
        let details
        if (errorHandler.validate(['email','password'], request.body))
            return apiResponder(request, response, next, true, 2003, {})

            const user = await User.findOne({
                where: { email: request.body.email, is_active: 1 }
            })
            if (user && user.is_verified == 1) {
                const passwordMatch = await bcrypt.compare(request.body.password, user.password)
    
                if (!passwordMatch) throw errorHandler.createError(1001)
    
                const token = await this.generateToken(
                    user.id,
                    user.email,
                    user.first_name,
                    'U'
                )

                details = {
                    id: user.id,
                    account_type: 'U',
                    auth_token: token
                }
    
            } else if (user && user.is_verified == 0) {
                await general.sendOtp(user.id, 'U')
                return apiResponder(request, response, next, true, 2006, {
                    id: user.id,
                    type: 'U'
                })
            }
            if(!user){
                let collectorDetail = await Collector.findOne({
                    where: {email: request.body.email, is_active: 1}
                })
                if(!collectorDetail){
                    return apiResponder(request, response, next, true, 2010, {})
                }
                if(collectorDetail && collectorDetail.is_verified == 1){
                    const passwordMatch = await bcrypt.compare(request.body.password, collectorDetail.password)
                    if (!passwordMatch) throw errorHandler.createError(1001)
    
                    const token = await this.generateToken(
                        collectorDetail.id,
                        collectorDetail.email,
                        collectorDetail.first_name,
                        'C'
                    )

                    details = {
                        id: collectorDetail.id,
                        account_type: 'C',
                        auth_token: token
                    }
                }
                
                if(collectorDetail && collectorDetail.is_verified == 0){
                    await general.sendOtp(collectorDetail.id, 'C')
                    return apiResponder(request, response, next, true, 2006, {
                        id: collectorDetail.id,
                        type: 'C'
                    })
                }
            }
            return apiResponder(request, response, next, true, 2000, details)
    }catch(error){
        next(error);
    }
}

exports.loginWithSocialMedia = async (request, response, next) =>{
        try{
            let details
            if (errorHandler.validate(['email'], request.body))
                return apiResponder(request, response, next, true, 2003, {})
    
                const user = await User.findOne({
                    where: { email: request.body.email, is_active: 1 }
                })
                if (user) {
                        const token = await this.generateToken(
                        user.id,
                        user.email,
                        user.first_name,
                        'U'
                    )
    
                    details = {
                        id: user.id,
                        account_type: 'U',
                        auth_token: token
                    }
        
                } 
                if(!user){
                    let collectorDetail = await Collector.findOne({
                        where: {email: request.body.email, is_active: 1}
                    })
                    if(!collectorDetail){
                        return apiResponder(request, response, next, true, 2010, {})
                    }
                    if(collectorDetail){
                       
                        const token = await this.generateToken(
                            collectorDetail.id,
                            collectorDetail.email,
                            collectorDetail.first_name,
                            'C'
                        )
    
                        details = {
                            id: collectorDetail.id,
                            account_type: 'C',
                            auth_token: token
                        }
                    }
                 }
                return apiResponder(request, response, next, true, 2000, details)
            
            }catch(error){
        next(error);
    }
}
exports.generateToken = async (id, email, name, type) => {
    try {
        const token = jwt.sign(
            {
                id: id, 
                email: email,
                name: name,
                type: type
            },
            `${process.env.CVM_SECRET_KET}`
        )
        if (token) return token
    } catch (error) {
        return error
    }
}